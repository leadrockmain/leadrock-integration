<?php
namespace Leadrock\Items;

use Leadrock\Layouts\Landing;
use Leadrock\Layouts\PreLanding;
use Leadrock\Layouts\ThankYou;

class Config
{
    const PARAM_LAYOUT = 'layout';
    const PARAM_THANKYOU_PAGE = 'thankyou_page';
    const PARAM_TRACK_ID_PARAM = 'track_id_param';
    const PARAM_TRACK_LINK = 'track_url';
    const PARAM_TRACK_LINK_IN_PARAM = 'track_url_in_param';
    const PARAM_API_KEY = 'api_key';
    const PARAM_API_SECRET = 'api_secret';
    const PARAM_SAVE_IN_FILE = 'save_in_file';
    const PARAM_FACAEBOOK_ID = 'facebook_id';
    const PARAM_FACEBOOK_GET_PARAM = 'facebook_from_param';
    const PARAM_FACEBOOK_LANDING_GOAL = 'facebook_landing_goal';
    const PARAM_FACEBOOK_THANKYOU_GOAL = 'facebook_thankyou_goal';
    const PARAM_FORM = 'form';

    const VALUE_LAYOUT_LANDING = 'landing';
    const VALUE_LAYOUT_PRELANDING = 'prelanding';
    const VALUE_TRACK_ID_PARAM = 'track_id';
    const VALUE_FACEBOOK_GET_PARAM = 'fbpixel';
    const VALUE_FACEBOOK_LANDING_GOAL = 'PageView';
    const VALUE_FACEBOOK_THANKYOU_GOAL = 'Purchase';
    const VALUE_TRACK_LINK_IN_PARAM = 'url';
    const VALUE_FORM = [
        'name' => 'name',
        'phone' => 'phone',
        'email' => 'email',
        'other' => 'other',
        'price' => 'price',
    ];

    private $params = [];

    public function __construct($filePath)
    {
        $this->params = include $filePath;
    }

    private static function getDefaultValues()
    {
        return [
            self::PARAM_LAYOUT => self::VALUE_LAYOUT_LANDING,
            self::PARAM_FACEBOOK_GET_PARAM => self::VALUE_FACEBOOK_GET_PARAM,
            self::PARAM_FACEBOOK_LANDING_GOAL => self::VALUE_FACEBOOK_LANDING_GOAL,
            self::PARAM_FACEBOOK_THANKYOU_GOAL => self::VALUE_FACEBOOK_THANKYOU_GOAL,
            self::PARAM_TRACK_ID_PARAM => self::VALUE_TRACK_ID_PARAM,
            self::PARAM_FORM => self::VALUE_FORM,
            self::PARAM_TRACK_LINK_IN_PARAM => self::VALUE_TRACK_LINK_IN_PARAM,
        ];
    }

    private function detDefaultValue($param)
    {
        $values = self::getDefaultValues();
        return isset($values[$param]) ? $values[$param] : null;
    }

    /**
     * @return string
     */
    public function getLayoutClass()
    {
        $layout = null;
        switch ($this->getParam(self::PARAM_LAYOUT)) {
            case self::VALUE_LAYOUT_LANDING:
                $layout = Landing::class;
                break;
            case self::VALUE_LAYOUT_PRELANDING:
                $layout = PreLanding::class;
                break;
        }

        if ($_SERVER['SCRIPT_FILENAME'] == $this->getParam(self::PARAM_THANKYOU_PAGE)) {
            $layout = ThankYou::class;
        }

        return $layout;
    }

    public function getParam($param)
    {
        return isset($this->params[$param]) ? $this->params[$param] : $this->detDefaultValue($param);
    }
}