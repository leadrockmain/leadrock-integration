<?php

namespace Leadrock\Layouts;

use Leadrock\Items\Lead;
use Leadrock\Helpers\Storage;
use Leadrock\Helpers\Url;


class UpdatingPage extends Landing
{
    private $thankyouUrl;
    private $fields = [
        'user_phone' => 'phone',
        'user_name' => 'name',
        'other' => 'other',
    ];

    public function beforeRender()
    {
        if (isset($_REQUEST[$this->urlParamName])) {
            if ($this->getMethod() != 'POST') {
                Storage::clearAll();
            }
            $this->setWebmasterLink((new Url($this))->createLinkFromPart(urldecode($_REQUEST[$this->urlParamName])));
        }

        if(isset($_GET['clearStorage'])) {
            Storage::clearAll();
        }

        if ($this->getTrackId(true) === null) {
            $this->debugLog('Need to add tracking script');
            $this->addTrackScript();
        } else {
            $this->debugLog('TrackId exists, no need in tracking script');
        }

        $this->appendFormValidator();

        if ($this->getMethod() == 'POST') {
            $this->updateLead();
            if (!empty($this->thankyouUrl)) {
                $this->redirect($this->thankyouUrl);
            }
        } elseif (isset($_REQUEST[self::RESTORE_PARAM_NAME]) && isset($_REQUEST[self::RESTORE_TIME_FROM_NAME]) && isset($_REQUEST[self::RESTORE_TIME_TO_NAME])) {
            $this->restoreLeads($_REQUEST[self::RESTORE_TIME_FROM_NAME], $_REQUEST[self::RESTORE_TIME_TO_NAME]);
            return false;
        }

        return Layout::beforeRender();
    }

    /**
     * Parsing form POST data and send it to providers
     */
    private function updateLead()
    {
        $this->debugLog('Updating lead');

        $lead = new Lead($this->getTrackId());
        $lead->setReferrer((new Url($this))->fullUrl());
        $lead->setBrowserParams($_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']);
        $this->debugLog('Lead data: ' . json_encode($lead->getFullData()));

        foreach ($this->leadSaveProviders as $provider) {
            $this->debugLog('Adding provider to lead: ' . get_class($provider));
            $lead->addProvider($provider);
        }

        foreach ($this->fields as $param => $field) {
            $lead->setParam($param, isset($_REQUEST[$field]) ? $_REQUEST[$field] : '');
        }
        $this->parseProviderResponse($lead->update());
    }

    /**
     * Applying response settings to user further actions
     *
     * @param $response
     */
    protected function parseProviderResponse($response)
    {
        $this->debugLog('Lead update response: ' . json_encode($response));

        if (!empty($response['facebook_id'])) {
            Storage::saveParam(Storage::FB_PIXEL_ID, $response['facebook_id']);
        }
        if (!empty($response['thankyou_url'])) {
            $this->thankyouUrl = $response['thankyou_url'];
        }
    }

    public function setThankyouPage($url)
    {
        $this->thankyouUrl = $url;
        return $this;
    }

}