<?php
include '../../vendor/autoload.php';

$integration = new \Leadrock\Layouts\Landing();
$integration
    ->addProvider(new \Leadrock\Providers\File('leads_log.txt'))
    ->addProvider(new \Leadrock\Providers\Leadrock('1278g3', 'so4ircshboiuerbvs'))
    ->findTrackIn('track_id')
    ->setFormField('user_name', 'name')
    ->setFormField('user_phone', 'phone')
    ->setFormField('other', 'other')
    ->setWebmasterLinkFromParam('track_url')
    ->setThankyouPage('upsell.php')
    ->addFacebookPixel((new \Leadrock\Items\FacebookPixel('123321123321'))->setEvent('PageView'));
include 'template.html';
$integration->end();
