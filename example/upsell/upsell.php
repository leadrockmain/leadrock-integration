<?php
include '../../vendor/autoload.php';

$integration = new \Leadrock\Layouts\UpdatingPage();
$integration
    ->addProvider(new \Leadrock\Providers\File('leads.log'))
    ->addProvider(new \Leadrock\Providers\Leadrock('1278g3', 'so4ircshboiuerbvs'))
    ->setFormField('other', 'other')
    ->setThankyouPage('thankyou.php')
    ->addFacebookPixel((new \Leadrock\Items\FacebookPixel('423322223321'))->setEvent('Upsell'))
;

include 'upsell_template.html';

$integration->end();