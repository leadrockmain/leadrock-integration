<?php
const TRACK_ID_PARAM_NAME = 'mc_leadrock_track_id';

$trackId = get_track_id();

$API = [
    'key' => 1,
    'secret' => 'asoeubasoiduw3ycopauwg'
];

/**
 * @param $post
 * @param $API
 */
function send_upsell($post, $API)
{
    $params = [
        'track_id' => $post['track_id'],
        'user_phone' => $post['phone'],
        'user_name' => $post['name'],
        'other' => $post['other'],
        'ip' => $_SERVER['REMOTE_ADDR'],
        'ua' => $_SERVER['HTTP_USER_AGENT'],
        'api_key' => $API['key'],
        'sub1' => $post['sub1'],
        'sub2' => $post['sub2'],
        'sub3' => $post['sub3'],
        'sub4' => $post['sub4'],
        'sub5' => $post['sub5'],
        'status' => 4,
    ];
    $url = 'https://leadrock.com/api/v2/lead/update';

    $params['sign'] = sha1(http_build_query($params) . $API['secret']);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
    curl_exec($ch);
    curl_close($ch);

    header('Location: ' . $post['success_page']);
}

/**
 * @return mixed|string
 */
function get_track_id()
{
    $trackId = '';

    if (!empty($_REQUEST['track_id'])) {
        $trackId = $_REQUEST['track_id'];
    }

    if (!empty($_COOKIE[TRACK_ID_PARAM_NAME])) {
        $trackId = $_COOKIE[TRACK_ID_PARAM_NAME];
    }

    if (!empty($_SESSION[TRACK_ID_PARAM_NAME])) {
        $trackId = $_SESSION[TRACK_ID_PARAM_NAME];
    }

    return $trackId;
}

if (!empty($_POST['track_id'])) {
    send_upsell($_POST, $API);
}
?>
<!-- Form example -->
<form id="form" class="form" method="post" action="">
    <label for="checkbox">Extended quantity</label>
    <input type="checkbox" id="checkbox" name="other[extended]" value="1">
    <input type="hidden" name="other[new_price]" value="83">
    <input type="hidden" name="other[new_quantity]" value="2">
    <input type="hidden" name="track_id" value="<?= $trackId ?>"/>
    <input type="hidden" name="success_page" value="thankyou.html"/>
    <button type="submit">Submit</button>
</form>
