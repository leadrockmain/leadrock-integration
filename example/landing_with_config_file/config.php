<?php
return [
    'layout' => 'landing',
    'thankyou_page' => 'thankyou.php',

    'track_param' => 'track_id',
    'track_url' => 'https://leadrock.com/URL-QWE12-RTY34',
    'track_url_in_param' => 'track_url',

    'facebook_id' => '123321123321,123321123322',
    'facebook_from_param' => 'fbid',
    'facebook_landing_goal' => 'PageView',
    'facebook_thankyou_goal' => 'Purchase',

    'api_key' => '1278g3',
    'api_secret' => 'o4ircshboiuerbvs',
    'save_in_file' => 'leads.log',

    'form' => [
        'user_phone' => 'phone',
        'user_name' => 'name',
        'other' => 'other',
    ]
];